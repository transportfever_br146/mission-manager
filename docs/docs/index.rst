*******************************
Welcome to the Mission Manager!
*******************************

The Thank-You-Patch added a long-awaited feature to `Transport Fever <http://transportfever.com>`_. No, I do not mean the reversing trains. Since now, you can *attach mission scripts to free games*. Sounds good, right? Well, in one game only exactly one mission script can be attached. Thus, two mission script mods can not be used simultaneously.

The "solution", well more a workaround, for this is the **Mission Manager**, shortly **MiMa**. It provides an interface to other mods through which the mission scripts of several mods are collected. The Mission Manager then hands them over to Transport Fever as one single big mission.
  
.. toctree::
   :maxdepth: 2
   :caption: Step by Step
   
   howto/index
   
.. toctree::
   :maxdepth: 2
   :caption: API
   
   api/index