local thisUtil = "BR146 MOD FILE LOADER v1.1"

require("stringutil")

local util = {}

-- TRACEBACK FUNCTIONS
function util.traceback(_verbose)
	local verbose = _verbose or 0
	
	local level = 1
	print(thisUtil, "INFO", "Preforming traceback...")
	while true do
		local info
		
		if(verbose) then
			info = debug.getinfo(level)
		else
			info = debug.getinfo(level, "Sl")
		end
		if not info then break end
		
		if info.what == "C" then   -- is a C function?
			print("", "", level, "C function")
		else   -- a Lua function
			print("", "", "", string.format("[%s]:%d", info.short_src, info.currentline))
			if verbose then
				for k,v in pairs(info) do print("", "", "", "", k,"=",v) end
			end
		end
		level = level + 1
	end
	print(thisUtil, "INFO", "Traceback complete!")
end

function util.getTracebackRoot(_verbose)
	local verbose = not (not _verbose)
	local info, nextInfo = nil
	local level = 1
	repeat
		info = nextInfo
		
		nextInfo = debug.getinfo(level, "S")
		level = level + 1
	until nextInfo == nil
	
	-- assume info ~= nil now
	if( info == nil or info.source == nil) then
		util.traceback(verbose)
		
		error(string.format("\nFailed to find root of traceback in '%s'.\n"..
			"Notify BR146 (https://www.transportfever.net/index.php/User/18313-BR146/), and provide following information:\n\n", thisUtil))
	end
	
	if string.starts(info.source, "@") then
		info.source = info.source:sub(2)
	end
	
	info.source = info.source:gsub("\\", "/")
	
	if verbose then
		if info.what == "C" then   -- is a C function?
			print(thisUtil, "INFO", "Traceback Root", "C function")
		else   -- a Lua function
			print(thisUtil, "INFO", "Traceback Root", info.source)
		end
	end
	
	return info.source
end

function util.packagePathToTable()
	local paths = {}
	for path in package.path:gmatch("[^;]+") do
		local root, base, modId, file = util.extractModRoot(path, true)
		if(root) then
			table.insert(paths, {root = root, modId = modId})
		end
	end

	return paths
end

-- MOD ROOT

-- return fullModPath, {pathToModsContainingFolder, mod_id}
function util.extractModRoot(filepath, continueOnError)
	local doFail = (continueOnError ~= true)
	local root = nil
	
	local function toRoot(...)
		local matches = {...}
		if(#matches == 3) then return matches else return nil end
	end

	root = root or toRoot(filepath:match("(.-)([^/]*_[0-9]*)/(strings%.lua)$"))
	root = root or toRoot(filepath:match("(.-)([^/]*_[0-9]*)/(mod%.lua)$"))
	root = root or toRoot(filepath:match("(.-)([^/]*_[0-9]*)/(filesystem%.lua)$"))
	root = root or toRoot(filepath:match("(.-)([^/]*_[0-9]*)/(res/.*)$"))
	root = root or toRoot(filepath:match("(.-)([^/]*_[0-9]*)/(config/.*)$"))
	
	root = root or toRoot(filepath:match("(.*/446800/)([0-9]*)/(strings%.lua)$"))
	root = root or toRoot(filepath:match("(.*/446800/)([0-9]*)/(mod%.lua)$"))
	root = root or toRoot(filepath:match("(.*/446800/)([0-9]*)/(filesystem%.lua)$"))
	root = root or toRoot(filepath:match("(.*/446800/)([0-9]*)/(res/.*)$"))
	root = root or toRoot(filepath:match("(.*/446800/)([0-9]*)/(config/.*)$"))

	if( root ) then
		return (root[1] .. root[2]), root[1], root[2], root[3]
	else
		if(doFail) then
			error(string.format("\nCould not extract mod root from [%s] in '%s'.\n"..
				"Notify BR146 (https://www.transportfever.net/index.php/User/18313-BR146/), and provide following information:\n\n",
				filepath, thisUtil))
		else
			print(thisUtil, "WARNING", "Ignoring failed mod root extraction.")
			return nil, nil, nil, nil
		end
	end
end

function util.getModRoot(continueOnError)
	local fullRootCallerFilePath = util.getTracebackRoot()

	return util.extractModRoot(fullRootCallerFilePath, continueOnError)
end

-- load file
function util.performActionAbsolute(file, actionFn, continueOnError, _errMsgs)
	local doFail = (continueOnError ~= true)
	local errMsgs = _errMsgs or {}
	
		
	--print(thisUtil, "INFO", "Loading file ["..file.."]...")
	local result = { pcall(actionFn, file) }
	--result[1] = true, wenn geladen
	--result[2...n] = Rückgabewert  / Fehlermeldung
	
	if( not result[1] ) then
		if ( doFail ) then
			error(string.format("\n[%s]: %s\n"..
				"Notify BR146 (https://www.transportfever.net/index.php/User/18313-BR146/), and provide following information:\n"..
				"Error: %s\n" , thisUtil, errMsgs[1] or "Failed to perform action.", result[2]))
		else
			print(thisUtil, "WARNING", errMsgs[2] or "Failed to perform action, but ignored:\n", "", result[2])
		end
	else
		--print(thisUtil, "INFO", "File loaded!")
	end
	
	if( doFail ) then
		table.remove(result, 1) -- remove success indication, always true
		return table.unpack(result)
	else
		return table.unpack(result)
	end
end


function util.performAction(fileRelativeToModRoot, actionFn, continueOnError, _errMsgs)
	local root = util.getModRoot(true) or ""
	local file = root..'/'..fileRelativeToModRoot
	return util.performActionAbsolute(file, actionFn, continueOnError, _errMsgs)
end

function util.load(fileRelativeToModRoot, continueOnError)
	return util.performAction(fileRelativeToModRoot, dofile, continueOnError, {
		"Failed to load file.",
		"Loading failed, but ignored:\n"
	})
end

function util.loadDefault(fileRelativeToModRoot, default)
	local result = {util.load(fileRelativeToModRoot, true)}
	
	if(result[1]) then
		table.remove(result, 1)
		return table.unpack(result)
	else
		return default
	end
end

function util.loadDataDefault(fileRelativeToModRoot, default)
	function dofileData(f)
		dofile(f)
		return data()
	end
	
	local result = {util.performAction(fileRelativeToModRoot, dofileData, true, {
		"Failed to load data file.",
		"Data loading failed, but ignored:\n"
	})}
	
	if(result[1]) then
		table.remove(result, 1)
		return table.unpack(result)
	else
		return default
	end
end

function util.loadRawAbsolute(file, continueOnError)
	
	local function doRawLoad(f)
		local fHndl,err = io.open(f, "r")

		local content = fHndl:read("*a")

		fHndl:close()
		return content
	end
	
	return util.performActionAbsolute(file, doRawLoad, continueOnError, {
		"Failed to load raw file.",
		"Raw loading failed, but ignored:\n"
	})
end

return util