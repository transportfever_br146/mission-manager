local modIdent = "br146_MissionManager"

local missionutil			= require "missionutil"
local TaskConcentrator		= require("br146_MissionManager.TaskConcentrator")
local MedalManager			= require("br146_MissionManager.MedalManager")
local LoadingScreenHandler	= require("br146_MissionManager.LoadingScreenHandler")
local defaultTaskCreator	= {
	waiting		= require("br146_MissionManager.DefaultTaskWaiting"),
	noPlugin	= require("br146_MissionManager.DefaultTaskNoPlugin"),
}

require("br146_MissionManager.SerializePatcher").patch()

-- class:
local MissionCreator = {}
MissionCreator.__index = MissionCreator

local function makeMissionInterfaceWarning()
	error("You are trying to load multiple mission mods at once. This is not possible.\nYou can ask for help in the forum www.tranportfever.net, as for @BR146.")
end
	
if missionutil.makeMissionInterface ~= makeMissionInterfaceWarning then
	MissionCreator._makeMissionInterface = missionutil.makeMissionInterface
	missionutil.makeMissionInterface = makeMissionInterfaceWarning -- forbid new missionInterface instances
end

-- methods:

local function modifyMissionInterface(mission)
	local callbacks = {}
	
	local function protect(t)
		return setmetatable({}, {
			__index = t,
			__newindex = function(t,k,v)
				print( string.format("Discard changes for key %q to value %q on table %q.", k, v, t) )
			end
		})
	end
	
	local function sortByOrder(t)
		table.sort(t, function( a, b )
			if a == nil and b == nil then return false end
			if a == nil then return false end
			if b == nil then return true end
			return a.order < b.order
		end)
	end
	
	mission.setCallback = function(typ, fn, options)
		if not callbacks[typ] then
			callbacks[typ] = {}
		end 
		
		
		local order = 0
		if type(options) == "table" then
			order = options.order or 0
		end
		
		if type(fn) == "function" then
			local hndl = protect({order = order, fn = fn, typ = typ})
			
			table.insert(callbacks[typ], hndl )
			sortByOrder(callbacks[typ])
			
			return hndl
		else
			error( string.format("Invalid callback given: type is %q", type(fn)) )
		end
	end
	
	mission.removeCallback = function( hndl )
		if callbacks[hndl.typ] then
			for k,v in ipairs(callbacks[hndl.typ]) do
				if v == hndl then
					table.remove(callbacks[hndl.typ], k)
					break
				end
			end
		end 
	end
		
	mission.runCallback = function (typ, data)
		if callbacks[typ] == nil then
			return nil
		else
			if typ == "JOURNAL_ENTRY" then
				data.original_amount = data.amount
				
				local gotChange = false
				for k, call in ipairs(callbacks[typ]) do	
					local pData = protect(data)
					local ret = call.fn(typ, pData)
					
					if ret then
						gotChange = true
						data.amount = ret
					end
				end
			
				return data.amount
			elseif typ == "TRACK_BUILDER" or typ == "STREET_BUILDER" then
				local response = nil
				local pData = protect(data)
				
				for k, call in ipairs(callbacks[typ]) do
					local ret = call.fn(typ, pData)
					
					if ret and not response then
						response = ret
					end
				end
			
				return response
			else
				print( string.format("Run requested for unknown callback type (%q)!", typ) )
				
				local pData = protect(data)
				
				for k, call in ipairs(callbacks[typ]) do
					call.fn(typ, pData)
				end
				
				return nil
			end
		end
	end

	local allowWrite = {
		save2 = true,
	}
	
	local apiSugar = {
		onInit		= "function",
		onInitGui	= "function",
		onLoad		= "function",
		onUpdate	= "function",
		onMarkerEvent = "function",
	}
	
	local meta = {
		__newindex = function(t, key, value)
			if allowWrite[key] == true then
				rawset(t, key, value)
			elseif value ~= nil and apiSugar[key] == type(value) then
				t[key](value)
			else
				error( string.format("Prohibited write attempt of value %q to key %q in mission!", tostring(value), tostring(key)) )
			end
		end,
		__index = mission,
		__metatable = nil
	}
	
	return setmetatable({}, meta)
end

function MissionCreator.new()
	local tc		= TaskConcentrator.new()
	local mission	= modifyMissionInterface(MissionCreator._makeMissionInterface(tc))
	local loadingScreenHandler = LoadingScreenHandler.load()
	
	tc:attachSaveHook( loadingScreenHandler:createSaveHook() )
	
	local creator = {
		medalManagers = {},
		TaskConcentrator = tc,
		Mission = mission,
		loadingScreenHandler = loadingScreenHandler,
	}
	
	return setmetatable(creator, MissionCreator)
end

function MissionCreator:getMission()
	return self.Mission
end

function MissionCreator:getTaskManager(modIdent)
	return self.TaskConcentrator:getTaskManager(modIdent)
end
	
function MissionCreator:getMedalManager(modIdent)
	local medalMngr = self.medalManagers[modIdent]
	if medalMngr then
		return medalMngr
	else
		medalMngr = MedalManager.new(modIdent)
		self.medalManagers[modIdent] = medalMngr
		return medalMngr
	end
end

function MissionCreator:getLoadingScreenHandler()
	return self.loadingScreenHandler
end

function MissionCreator:validateMission()
	local mission		= self:getMission()
	local taskManager	= self:getTaskManager(modIdent)
	local medalMngr		= self:getMedalManager(modIdent)
		
	-- To validate: is at least one task added?	
	if self.TaskConcentrator:numberOfRegisteredTasks() <= 0 then	
		defaultTaskCreator.noPlugin(mission, taskManager, medalMngr)		
	end
	
	-- Add Waiting Task (to prevent crash, if plugin tasks are registered, but not added)
	self.TaskConcentrator:setWaitingTaskAccessor(defaultTaskCreator.waiting(mission, taskManager, medalMngr))

	return mission
end

function MissionCreator:validateMedals()
	return MedalManager.listMedals(self.medalManagers)
end

return MissionCreator