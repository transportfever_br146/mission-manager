local modIdent = "br146_MissionManager"

local defaultScreenData = require("br146_MissionManager.DefaultLoadingScreen")
local serializePatcher = require("br146_MissionManager.SerializePatcher")

local LoadingScreenHandler = {
	path =  "res/config/",
	filename = modIdent .. "@LoadingScreenData.lua",
}

LoadingScreenHandler.__index = LoadingScreenHandler

local function isTable(t)
	return type(t) == "table"
end

local function add_string(tab, addData)
	if isTable(addData) then
		for k, i in pairs(addData) do
			add_string(tab, i)
		end
	elseif type(addData) == "string" then
		table.insert(tab, addData)
	end
end

-- Data Set
local LoadingDataSet = {}
LoadingDataSet.__index = LoadingDataSet

function LoadingDataSet.new()
	local set = { img = {}, txt = {}, poster = {} }
	return setmetatable(set, LoadingDataSet)
end

function LoadingDataSet:addTxt(txt)
	add_string(self.txt, txt)
end
	
function LoadingDataSet:addImg(img)
	add_string(self.img, img)
end

function LoadingDataSet:addPoster(pst)
	if isTable(pst) then
		if type(pst.img) == "string" and type(pst.txt) == "string" then
			table.insert(self.poster, pst)
		else
			for k, child in pairs(pst) do
				self:addPoster(child)
			end
		end
	end
end


function LoadingDataSet:hasTxt()
	return #self.txt > 0
end

function LoadingDataSet:hasImg()
	return #self.img > 0
end
	
function LoadingDataSet:hasPoster()
	return #self.poster > 0
end

function LoadingDataSet:getRandomTxt()
	return self.txt[ math.random( #self.txt ) ]
end

function LoadingDataSet:getRandomImg()
	return self.img[ math.random( #self.img ) ]
end

function LoadingDataSet:getRandomPoster()
	return self.poster[ math.random( #self.poster ) ]
end

function LoadingDataSet:getRandom()
	local numberPoster = self:numPoster()
	local numberTxtImg = self:numImg() * self:numTxt()
	local numberTotal  = numberPoster + numberTxtImg
	
	if numberTotal <= 0 then
		-- nothing available
		return false, { img = nil, txt = nil }
	else
		local probPoster = numberPoster / numberTotal
		local r = math.random()
		
		if r < probPoster then
			-- random poster
			return true, self:getRandomPoster()
		else
			-- random text/img pair
			-- Note: at least one text and one img are available
			
			return true, {
				img = self:getRandomImg(),
				txt = self:getRandomTxt()
			}
		end
	end
	
end

function LoadingDataSet:numTxt()
	return #self.txt
end

function LoadingDataSet:numImg()
	return #self.img
end

function LoadingDataSet:numPoster()
	return #self.poster
end

function LoadingDataSet:addData(data)
	if isTable(data) then
		self:addTxt(data.txt)
		self:addImg(data.img)
		self:addPoster(data.poster)
	end
end

function LoadingDataSet:serialize()
	local str = "{img={"
	
	for i, img in ipairs(self.img) do
		str = str .. string.format("%q,", img)
	end
	
	str = str .. "},txt={"

	for i, txt in ipairs(self.txt) do
		str = str .. string.format("%q,", txt)
	end
	
	str = str .. "},poster={"

	for i, poster in ipairs(self.poster) do
		str = str .. string.format("{img=%q,txt=%q},", poster.img, poster.txt)
	end
	
	str = str .. "}}"
	return str
end

-- End Data Set

function LoadingScreenHandler.load()
	local result = {
		mods = LoadingDataSet.new(),
		loaded = LoadingDataSet.new(),
		default = LoadingDataSet.new(),
	}
	
	result.default:addData( defaultScreenData )
	
	package.path = LoadingScreenHandler.path .. "?.lua;" .. package.path
	
	local success, data = pcall(require, LoadingScreenHandler.filename:sub(1, -5))
	
	if success then
		result.loaded:addData(data)
	end
	
	result = setmetatable(result, LoadingScreenHandler)
	return result
end

function LoadingScreenHandler:createSaveHook()
	local this = self
	return function()
		local code = 
string.format([[local f=io.open(%q,"w");f:write("return"..%q);f:close();]], LoadingScreenHandler.path .. LoadingScreenHandler.filename, this:serialize() )
		
		return serializePatcher.createFunctionCall(code)
	end
end

function LoadingScreenHandler:serialize()
	return self.mods:serialize()
end

function LoadingScreenHandler:clearLoaded()
	self.loaded = LoadingDataSet.new()
	
	self:save()
end


function LoadingScreenHandler:save()
	local f = io.open(LoadingScreenHandler.path .. LoadingScreenHandler.filename, "w");
	
	f:write("return ");
	f:write(self:serialize())
	f:close();
end

function LoadingScreenHandler:addLoadingScreenData(potentialData)
	self.mods:addData(potentialData)
	self:clearLoaded()
end

function LoadingScreenHandler:getRandom()
	local success, lsp

	success, lsp = self.mods:getRandom()
	if success then return lsp end
	
	success, lsp = self.loaded:getRandom()
	if success then return lsp end
	
	success, lsp = self.default:getRandom()
	if success then return lsp end
	
	return { img = "../menu_new_background_1.tga", txt = "Hi! Ähm... How are you?\nSomehow, the loading screen hints were not loaded for this mission?" }
end

return LoadingScreenHandler.load()