local MedalManager = {}
MedalManager.__index = MedalManager


function MedalManager.new(modIdent)
	local med = {
		medals = {},
		modIdent = modIdent
	}
	
	return setmetatable(med, MedalManager)
end

function MedalManager:createMedal(name, description)
	local medalNo = #self.medals + 1
	local id = self.modIdent .. "@" .. tostring(medalNo)
	local med = {
		id = id,
		name = name,
		description = description,
	}
	
	self.medals[medalNo] = med
	return id
end

function MedalManager.listMedals(medalManagers)
	local medals = {}
	
	for modIdent, medMan in pairs(medalManagers) do
		for i, med in ipairs(medMan.medals) do
			table.insert(medals, med)
		end
	end
	return medals
end

return MedalManager