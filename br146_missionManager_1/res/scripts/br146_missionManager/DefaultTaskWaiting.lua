local mod = (require "br146_mod_strings_v2_3")

local str = {
	title	= mod.defaultTask.waiting.title(),
	desc	= mod.defaultTask.waiting.desc(),
	
}

local missionutil = require("missionutil")

return function(mission, taskManager)
	local TASK_ID = "defaultWaitingTask"

	local function makeDefaultWaitingTask()
			
		local info = {
			name = str.title,
			paragraphs = {
				{ text = str.desc },	
			},
			options = {},
			visible = false,
		}

		local task = missionutil.makeTask(info)

		task.setProgressNone()

		return task
	end

	
	taskManager:register(TASK_ID, makeDefaultWaitingTask )
	
	mission.onInit = function()
		taskManager:add(TASK_ID)
	end
	
	return function()
		return taskManager:getById(TASK_ID)
	end
end
