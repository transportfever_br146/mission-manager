local mod = (require "br146_mod_strings_v2_3")

local data = { txt = {}, img = {}, posters = {} }

for k, txtFn in pairs(mod.defaultLoadingScreen.text) do
	local txt = string.gsub(txtFn(), "\n", "")
	table.insert( data.txt, txt)
end
	
for k, imgFn in pairs(mod.defaultLoadingScreen.img) do
	table.insert( data.img, imgFn() )
end

return data