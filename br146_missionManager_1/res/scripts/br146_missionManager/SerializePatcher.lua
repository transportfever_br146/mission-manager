local serializePatcher = {
	searchPattern = [[^(%s*)(error%(%s*"invalid metatag: "%s*%.%.%s*metatag%s*%))%s*$]],
	replacement = [[%1if metatag == "_run_" then
%1	writeFn("(function() ")
%1	writeFn(o.val)
%1	writeFn(" end)()")
%1else
%1	%2 -- PATCHED BY BR146 Mission Manager
%1end]],
	file = "res/scripts/serialize.lua",
	patched = false
}

function serializePatcher.write(codeLines)
	file = io.open(serializePatcher.file, "w")
	
	for no, line in ipairs(codeLines) do
		file:write( line )
	end
	file:flush()
	file:close()
end

function serializePatcher.validate()
	local success, err = pcall(dofile, serializePatcher.file)
	
	if not success then
		print( string.format("Validation of %q failed: %s", serializePatcher.file, err) )
	end
		
	return success
end

-- PATCH FN
function serializePatcher.patch()
	local codeLinesNew = { }
	local codeLinesOld = { }
	
	for line in io.lines(serializePatcher.file) do
		table.insert( codeLinesOld, line .. '\n' )
		table.insert( codeLinesNew, string.gsub(line, serializePatcher.searchPattern, serializePatcher.replacement) .. '\n' )
	end
	
	local success, err = pcall( serializePatcher.write, codeLinesNew)
	if not success then
		print( string.format("Error while patching %q: %s", serializePatcher.file, err) )
	end
	
	if true == serializePatcher.validate() then
		serializePatcher.patched = true
	else
		-- revert patch
		local success, err = pcall( serializePatcher.write, codeLinesOld)
		if not success then
			print( string.format("Error while patching %q: %s", serializePatcher.file, err) )
		end
		
		print( string.format("Failed to patch %q", serializePatcher.file) )
		serializePatcher.patched = false
	end
end

function serializePatcher.createFunctionCall(functionCode)
	if serializePatcher.patched then
		return {
			["__metatag__"] = "_run_",
			val = functionCode
		}
	else
		return "_serialize_was_not_patched_"
	end
end

return serializePatcher