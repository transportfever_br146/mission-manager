local privateStateId = "__br146_mission_manager_intern__"

local TaskManagerProxy = require("br146_MissionManager.TaskManagerProxy")

local TaskConcentrator = {}
TaskConcentrator.__index = TaskConcentrator

function TaskConcentrator.new()
	local concentrator = {
		taskManagers = {},
		saveStateHooks = {},
		waitingTaskAccessor = function() return nil end,
	}
	
	return setmetatable(concentrator, TaskConcentrator)
end

function TaskConcentrator:getTaskManager(modIdent)
	local tm = self.taskManagers[modIdent]
	
	if tm then
		return tm
	else
		tm = TaskManagerProxy.new(modIdent, function() self:validateTaskState() end )
		self.taskManagers[modIdent] = tm
		return tm
	end
end


function TaskConcentrator:initGui()
	for modIdent, tm in pairs(self.taskManagers) do
		tm:initGui()	
	end
end

function TaskConcentrator:attachSaveHook(hook)
	table.insert(self.saveStateHooks, hook)
end

function TaskConcentrator:save()
	local state = {
		[privateStateId] = {}
	}
	
	for i, hook in ipairs(self.saveStateHooks) do
		table.insert( state[privateStateId], hook() )
	end
	
	for modIdent, tm in pairs(self.taskManagers) do
		state[modIdent] = tm:save()	
	end
	return state
end

function TaskConcentrator:load(state)
	for modIdent, tm in pairs(self.taskManagers) do
		tm:load(state[modIdent])	
	end
end

function TaskConcentrator:update()
	for modIdent, tm in pairs(self.taskManagers) do
		tm:update()	
	end
end

function TaskConcentrator:getState()
	return "RUNNING" -- Mission never ends :D
end

function TaskConcentrator:getUserState(modIdent)
	if self.taskManagers[modIdent] then
		return self.taskManagers[modIdent]:getUserState()
	else
		local userStates = {}
		for modIdent, tm in pairs(self.taskManagers) do
			userStates[modIdent] = tm:getUserState()
		end

		return userStates
	end
end

function TaskConcentrator:setWaitingTaskAccessor(accessorFn)
	self.waitingTaskAccessor = accessorFn
	
	self:validateTaskState()
end

function TaskConcentrator:validateTaskState()
	local waitingTask = self.waitingTaskAccessor()
	
	if not waitingTask then
		return
	end
	
	local tasks = self:getAllTaskData()
	
	local anyIncompleteTaskVisible = false
	
	for i, task in ipairs(tasks) do
		print(task.task.getInfo().name)
		if task.task ~= waitingTask then
			if task.task.isVisible() and not task.task.isCompleted() then
				anyIncompleteTaskVisible = true
				break
			end
		end
	end

	waitingTask.setVisibleRaw( not anyIncompleteTaskVisible )
end

function TaskConcentrator:getTasks()
	local tasks = {}
	
	for modIdent, tm in pairs(self.taskManagers) do
		for k, t in ipairs(tm:getTasks()) do
			table.insert(tasks, t)
		end
	end
	
	return tasks
end

function TaskConcentrator:getAllTaskData()
	local tasks = {}
	
	for modIdent, tm in pairs(self.taskManagers) do
		for k, t in ipairs(tm:getAllTaskData()) do
			table.insert(tasks, t)
		end
	end
	return tasks
end

function TaskConcentrator:getById(taskId)
	for modIdent, tm in pairs(self.taskManagers) do
		local t = tm:getById(taskId)
		if t then return t end
	end
	
	return self.waitingTaskAccessor()
end

function TaskConcentrator:numberOfRegisteredTasks()
	local c = 0
	for modIdent, tm in pairs(self.taskManagers) do
		c = c + tm:numberOfRegisteredTasks()
	end
	return c
end

return TaskConcentrator