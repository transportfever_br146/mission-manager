local mod = (require "br146_mod_strings_v2_3")

local str = {
	title	= mod.defaultTask.noPlugin.title(),
	desc	= mod.defaultTask.noPlugin.desc(),
	hint	= mod.defaultTask.noPlugin.hint(),
}

local missionutil = require("missionutil")

return function(mission, taskManager)	
	local TASK_ID = "defaultTaskNoPlugin"
	
	local function makeDefaultTaskNoPlugin()
			
		local info = {
			name = str.title,
			paragraphs = {
				{ text = str.desc }, 
				{ type = "HINT", text = str.hint },			
			},
			options = {},
			saveable = false,
		}

		local task = missionutil.makeTask(info)

		task.setProgressNone()

		return task
	end

	
	taskManager:register(TASK_ID, makeDefaultTaskNoPlugin )
	
	return taskManager:prefix(TASK_ID), taskManager:add(TASK_ID)
end
