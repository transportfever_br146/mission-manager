-- Hijack serializer function

require("serialize")


-- Initialize global variables, if not done yet

local gameCreation = (game == nil)
if gameCreation then
	game = {}
end

if game.br146==nil then
	game.br146 = {}
end

if game.br146.MissionManager == nil then
	game.br146.MissionManager = require("br146_MissionManager.MissionCreator").new(not gameCreation)
end

-- provide interface
local missionutil = require "missionutil"
local fileutil = require "br146_mod_fileloader_v1_1"

-- API v1.0
local API_1_0 = {}

function API_1_0.register(modIdent, missionCreationSource, options)
	local mission		= game.br146.MissionManager:getMission()
	local loadingScreen = game.br146.MissionManager:getLoadingScreenHandler()
	local taskManager	= game.br146.MissionManager:getTaskManager(modIdent)
	local medalManager	= game.br146.MissionManager:getMedalManager(modIdent)
	
	local missionCreationFunction
	
	if type(missionCreationSource) == "function" then
		missionCreationFunction = missionCreationSource
	elseif type(missionCreationSource) == "string" then
		local defaultRet = function() end
		local data = fileutil.load(missionCreationSource, defaultRet)
		
		if type(data) == "function" then
			if data ~= defaultRet then
				missionCreationFunction = data
			else
				error( string.format("An error occured while loading the script file %q:\n", missionCreationSource, data))
			end
		else
			error( string.format("Your script file %q did not return a function, it is of type %q. We can not handle that!", missionCreationSource, type(data)))
		end
	else
		error( string.format("The source for creating mission for %q is of type %q?! We can not handle that!", modIdent, type(missionCreationSource) ))
	end
	
	-- option handling
	
	if type(options) == "table" then
		loadingScreen:addLoadingScreenData({ txt = options.loadingScreenHints, img = options.loadingScreenImages, poster = options.loadingScreenPosters })
	end
	
	-- create Mission
	
	local result = {pcall(missionCreationFunction, mission, taskManager, medalManager)}
	local err = not result[1]
	if err then
		error( string.format("An error occured while creating mission for %q:\n%s", modIdent, result[2]) )
	else
		return table.unpack(result)
	end
end


-- RETURN API
return setmetatable({
	API_1_0 = API_1_0,
	
	current = "API_1_0",
}, {
	__index = function(api_table, key)
		local raw = rawget(api_table, key)
		if raw then
			return raw
		else
			error( string.format("A higher version of the Mission Manager API is requested by a Plugin.\nRequested %s, installed %s. Please update the Mission Manager", key, api_table.current))
		end
	end,
	__newindex = function(api_table, key, val)
		print( string.format("The API table is protected. It is not allowed to write to it! (Tried to access %q to write %q", key, val) )
	end,
	__metatable = nil,
})