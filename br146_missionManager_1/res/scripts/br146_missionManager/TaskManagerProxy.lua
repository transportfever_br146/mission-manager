local TaskManager = require "TaskManager"

local prefixTable = {
	-- NOTE: do not forget to count the "self" variable (the ":" also counts!)
	["gen"] = { 2, 3 },
	["setZone"] = { 2 },
	["setMarker"] = { 2 },
}

local TaskManagerProxy = {}

TaskManagerProxy.__index = function(t, key)	
	local raw = rawget(t, key) or rawget(TaskManagerProxy, key)

	if raw ~= nil then
		return raw
	end

	raw = t.taskManager[key]
	if(type(raw) == "function") then
		return function(...)
			local args = {...}
			
			-- redirect self, if available
			if(args[1] == t) then
				args[1] = t.taskManager
			end
			
			local doPrefix = prefixTable[key] or {}
			
			for k, index in ipairs(doPrefix) do
				args[index] = t:prefix(args[index])
			end
			
			return raw(table.unpack(args))
		end
	else
		return raw
	end
end

function TaskManagerProxy.new(modIdent, taskChangeCallback)
	local tm = TaskManager.new()
	
	local proxy = {
		modIdent = modIdent,
		taskManager = tm,
		tasksRegistered = 0,
		taskChangeCallback = taskChangeCallback,
	}
	
	return setmetatable(proxy, TaskManagerProxy)
end


function TaskManagerProxy:prefix(str)
	if str~=nil then
		return self.modIdent .. "@" .. tostring(str)
	else
		return nil
	end
end

function TaskManagerProxy:numberOfRegisteredTasks()
	return self.tasksRegistered
end

local function proxyTaskManagerToFunction(originalFunction, newTaskManager, argumentNo)
	return function(...)
		local args = {...}
		args[argumentNo] = newTaskManager
		return originalFunction(table.unpack(args))
	end
end

function TaskManagerProxy:register(taskName, fn, ...)
	local thisTaskManager= self
	local prefixed_taskName = self:prefix(taskName)
	
	local function createTaskWithTaskManagerProxy(...)
		local task = fn(...)
		
		task.handleEvent	= proxyTaskManagerToFunction( task.handleEvent, thisTaskManager, 1)
		
		-- Not needed: update is directly called by individual TM
		-- task.update		= proxyTaskManagerToFunction( task.update, thisTM, 1)

		-- proxy visible state
		local infoTable = task.getInfo()
		task.getProgress().visible = infoTable.visible
		task.getProgress().saveable = infoTable.saveable
		task.getProgress().removed = false
		task.getProgress().optionsAvailable_raw = task.getProgress().optionsAvailable
		
		infoTable.visible = nil
		infoTable.saveable = nil
		
		task.getInfo = function()
			return setmetatable(infoTable, {
				__index = function(t, key)
					if key == "visible" then
						return task.isVisible()
					else
						return rawget(infoTable,key)
					end
				end,
				__newindex = function(t, key, value)
					if key == "visible" then
						task.setVisible(value)
					else
						rawset(infoTable, key, value)
					end
				end
			})
		end
		
		local setCompletedOrig = task.setCompleted
		task.setCompleted = function(subTask)
			setCompletedOrig(subTask)
			thisTaskManager.taskChangeCallback()
		end
		
		local function updateOptionsAvailable()
			local process = task.getProgress()
			process.optionsAvailable = process.optionsAvailable_raw and task.isVisible()
		end
		
		-- new API

		task.setVisible = function(isVisible)
			task.setVisibleRaw(isVisible)
			
			thisTaskManager.taskChangeCallback()
		end
		
		task.isVisible = function()
			return task.getProgress().visible and not task.isRemoved()
		end
		
		task.isRemoved = function()
			return task.getProgress().removed
		end
		
		task.setOptionsAvailable = function(value)
			task.getProgress().optionsAvailable_raw = value
			
			updateOptionsAvailable()
		end
		
		task.areOptionsAvailable = function()
			return task.getProgress().optionsAvailable_raw
		end
		
		
		-- private
		task.setVisibleRaw = function(isVisible)
			task.getProgress().visible = isVisible
			
			updateOptionsAvailable()
		end
		
		task.markRemoved = function()
			task.getProgress().removed = true
			
			updateOptionsAvailable()
			
			thisTaskManager.taskChangeCallback()
		end
		
		task.setSaveable = function(saveable)
			task.getProgress().saveable = saveable
		end
		
		task.isSaveable = function()
			if task.getProgress().saveable ~= nil then
				return task.getProgress().saveable
			else
				return true
			end
		end
		
		return task
	end
		
	self.taskManager:register(prefixed_taskName, createTaskWithTaskManagerProxy, ...)
	self.tasksRegistered = self.tasksRegistered + 1
end

function TaskManagerProxy:add(taskName, data, taskId)
	local prefixed_taskName = self:prefix(taskName)
	local prefixed_taskId = self:prefix(taskId or taskName)
	
	local result = self.taskManager:add( prefixed_taskName, data, prefixed_taskId )
	self.taskChangeCallback()
	return result
end

function TaskManagerProxy:remove(taskInstance)
	taskInstance.markRemoved()
end

function TaskManagerProxy:performRemovalsNow()
	for i = #self.taskManager.tasks,1,-1 do
		local iTask = self.taskManager.tasks[i]
		if iTask.task.isRemoved() then
			self.taskManager.taskIds[iTask.id] = nil
			table.remove(self.taskManager.tasks, i)
		end
	end
end

function TaskManagerProxy:getById(id)
	return self.taskManager:getById(self:prefix(id)) or self.taskManager:getById(id) 
end


function TaskManagerProxy:getAllTaskData()
	return self.taskManager.tasks
end

function TaskManagerProxy:save()
	local result = { }

	for i = 1, #self:getAllTaskData() do
		local t = self.tasks[i]
		if t.task.isSaveable() and not t.task.isRemoved() then
			result[#result + 1] = { name = t.name, id = t.id, data = t.task.save() }
		end
	end

	return { self.state, result }
end

return TaskManagerProxy
