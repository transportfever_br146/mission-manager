local thisUtil = "BR146 MOD STRINGS v2.3"

local modFileLoader = require("br146_mod_fileloader_v1_1")

-- placeholder handling
local function loadPlaceholders(modRoot)
	return {
		-- everything in lower case, en must exist in tables!
		lb = {
			before = "",
			after = "",
			en = "\n",
		},
		abs = {
			before = "",
			after = "",
			en = "\n\n",
		},
		coffee  = modFileLoader.loadDefault("config/coffee.lua",  "\n\nWhere's my coffee??? Something went wrong loading text! AHHH! I need coffee!!!"),
		licence = modFileLoader.loadDefault("config/licence.lua", "\n\nMhhhh. Here should be a licence text? o.O")
	}
end


local function replacePlaceholders(text, lang, placeholderData)
	local newText, count = string.gsub(text, "(%s?)%%(%w+)%%(%s?)", function(leadingSpace, placeholder, trailingSpace)
			local data = placeholderData[string.lower(placeholder)]
			local content = ""
			
			if not data then
				return
			elseif type(data) == "string" then
				content = data
			elseif type(data) == "table" then
				content = data[lang] or data["en"] or ""
			end
			
			-- handle leading and trailing spaces
			content = (data.before or leadingSpace or "") .. content .. (data.after or trailingSpace or "") 

			return content
		end)
		
	if count > 0 then
		return replacePlaceholders(newText, lang, placeholderData) -- handle possible placeholders in placeholders
	else
		return newText
	end
end

-- create strings.lua

local function handleTranslationTable(ident, translationTable, stringsLuaData, placeholderData)
	for lang, text in pairs(translationTable) do
		if(stringsLuaData[lang] == nil) then
			stringsLuaData[lang] = {}
		end
			
		stringsLuaData[lang][ident] = replacePlaceholders(text, lang, placeholderData):gsub('\t',"")
	end
end

local function getNestedTranslation(ident, t, stringsLuaData, keyTable, placeholderData)
	local isTranslationTable = true
	for k,v in pairs(t) do
		isTranslationTable = isTranslationTable and type(v) ~= "table"
	end
	
	if(isTranslationTable) then
		handleTranslationTable(ident, t, stringsLuaData, placeholderData)
	else
		for key,nextT in pairs(t) do
			keyTable[key] = setmetatable({}, {
				__tostring = function(t)
					return tostring(keyTable).."@"..key
				end
			})
			getNestedTranslation(ident.."@"..key, nextT, stringsLuaData, keyTable[key], placeholderData)
		end
	end
end

local function createStringsLua(mystrings)
	local root, path, modId = modFileLoader.getModRoot(true)
		
	if(modId) then
		local placeholderData = loadPlaceholders(root)

		local stringsLuaData = {}
		local keyTable = {}
		
		getNestedTranslation(modId, mystrings, stringsLuaData, keyTable, placeholderData)
	
		return modId, stringsLuaData, keyTable
	else			
		print(thisUtil, "ERROR", "Failed to get mod id. Can not create strings.lua...")
		return nil, {}
	end
end

-- String access handlers
local function get(key)
	local full, path, modId = modFileLoader.getModRoot(true)
	
	if(modId) then
		return _(modId.."@"..key)
	else
		print(thisUtil, "ERROR", "Failed to get mod id. Returning error text...")
		return "<MOD ID NOT FOUND / KEY="..key..">"
	end
end


-- Build access table
local br146_mod_strings = {}
local hook_meta

local function makeHook(thisKey, ...)
	local keys = {...}
	table.insert(keys, thisKey)
	return setmetatable({ ["__meta__"] = keys}, hook_meta)
end

hook_meta = {
	__index = function(hook, nextKey)
		return makeHook( nextKey, table.unpack( rawget(hook,"__meta__") ) )
	end,
	
	__call = function(hook)
		return get(tostring(hook))
	end,
	
	__tostring = function(hook)
		return table.concat( rawget(hook,"__meta__"), "@")
	end,
		
	__pairs = function (hook)
		local full, path, modId = modFileLoader.getModRoot(true)
		
		local function stateless_iter(tbl, k)
			local v
			k, v = next(tbl, k)
			if k then return k, hook[k] end
		end
		
		if not modId then
			return stateless_iter, {}, nil
		end
		
		local modStr = br146_mod_strings[modId]
		if not modStr then
			return stateless_iter, {}, nil
		end
		
		for k, key in ipairs( rawget(hook,"__meta__") ) do
			modStr = modStr[key]
			if not modStr then
				return stateless_iter, {}, nil
			end
		end
		
		-- Return an iterator function, the table, starting point
		return stateless_iter, modStr, nil
	end
}

return setmetatable({ ["__meta__"] = {} }, {
	__index = hook_meta.__index,
	__pairs = hook_meta.__pairs,
	__tostring = hook_meta.__tostring,
	
	__call = function(t, mystrings)		
		local modId, stringsLuaData, keyTable = createStringsLua(mystrings)
		
		if modId then
			if br146_mod_strings[modId] == nil then br146_mod_strings[modId] = {} end
			
			br146_mod_strings[modId] = keyTable
		end
		
		return stringsLuaData
	end
})