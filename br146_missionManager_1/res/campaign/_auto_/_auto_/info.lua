local mod = require "br146_mod_strings_v2_3"
		
function data()
	require("br146_MissionManager.MissionManager")
	
	local medals = game.br146.MissionManager:validateMedals()
	
	local loadingScreenHandler = game.br146.MissionManager:getLoadingScreenHandler()
	local loadingData = loadingScreenHandler:getRandom()
	
	return {
		name = mod.mission.name(),
		description = _(loadingData.txt),
		image = loadingData.img,
		script = "_auto_/_auto_/main.lua",
		medals = medals,
		voiceOver = "",
	}
end
