local mod = require "br146_mod_strings_v2_3"

function data()
return {
	name = mod.mission.group(),
	icon = "",
	description = mod.mission.desc(),
	order = 10,
}
end
