local makeStringLua = require "br146_mod_strings_v2_3"

function data()
	local s = makeStringLua({
		name = {
			de = "Mission Manager (MiMa)",
			en = "Mission Manager (MiMa)"
		},
		desc = {
			de = "In Transport Fever können leider nicht mehrere Missionsmods gleichzeitig in einem Endlosspiel genutzt werden. Der Mission Manager, kurz MiMa, gibt die Möglichkeit, diese Limitierung zu umgehen. Von anderen Moddern erstellte \"MiMa Plugins\" können sich mit ihren Missionen beim MiMa registrieren, welcher die Missionen Transportfever aufbereitet einflöst. Auf diese Art können mehrere Missionen und Aufgaben parallel genutzt werden. %coffee% %licence%",
			en = "Transport Fever does not support attaching multiple mission scripts from different mods to a single free game. The Mission Manager - or in short MiMa - is a basic script mod, which bypasses this restriction. So-called \"MiMa Plugins\" can be created by other modders, which registers their missions at the MiMa which instills the missions to Transport Fever. This way, multiple missions with multiple tasks can be used in parallel. %coffee% %licence%",
		},
		author = {
			de = "Autor: BR146",
			en = "author: BR146"
		},
		mission = {
			group = {
				de = "Transportfever.net / BR146",
				en = "Transportfever.net / BR146",
			},
			name = {
				de = "Mission Manager",
				en = "Mission Manager",
			},
			desc = {
				de = "Ermöglicht mehrere Mod-Missionen auf einmal.",
				en = "Enables the use of multiple mod mission scripts.",
			},
		},
		defaultTask = {
			waiting = {
				title = {
					de = "Keine Aufgaben.",
					en = "No Tasks.",
				},
				desc = {
					de = "Gerade sind keine Aufgaben verfügbar. Warte einfach ab, es werden welche erscheinen.",
					en = "Currently, no tasks are available. They will show up over time.",
				},
			},
			noPlugin = {
				title = {
					de = "Keine Mission Manager Plugins geladen.",
					en = "No plugins for Mission Manager loaded.",
				},
				desc = {
					de = "Es können zusätzliche Plugins heruntergeladen werden, welche herausfordernde Aufgaben und Werkzeuge zur Verfügung stellen.",
					en = "You can download plugins with different challenges and tools.",
				},
				hint = {
					de = "Lade Plugins von transportfever.net!",
					en = "Download them at transportfever.net!",
				},
			}
		},
		defaultLoadingScreen = {
			text = {
				hint1 = {
					de = "Lade Plugins von transportfever.net!",
					en = "Download them at transportfever.net!",
				},
				hint2 = {
					de = "%coffee% !",
					en = "%coffee% !",
				},
			},
			img = {
				img0 = {en = "../menu_new_background_0.tga"},
				img1 = {en = "../menu_new_background_1.tga"},
				img2 = {en = "../menu_new_background_2.tga"},
				img3 = {en = "../menu_new_background_3.tga"},
				img4 = {en = "../menu_new_background_4.tga"},
				img5 = {en = "../menu_new_background_5.tga"},			
			}
		}
	})			
	
	return s
end