local mod = require "br146_mod_strings_v2_3"
		
function data()
  return {
  
	info = {
		minorVersion = 0,
		severityAdd = "WARNING",
		severityRemove = "CRITICAL", 
		name = mod.name(),
		description = mod.desc(),

		authors = {
			{
				name = 'BR146',
				role = 'CREATOR',
				text = '',
				steamProfile = '76561198146230194',
				tfnetId = '18313'
			},
		},
		
		tfnetId = 0,
		tags = { "Mission", "Script Mod" },
		
		visible = true,
	},
	
	runFn = function()
		-- To have it loaded at least once.
		require("br146_MissionManager.MissionManager")
		
		-- All the runFn's are running. So there must be eigther data added by other mods or the defaults should be used.
		require("br146_MissionManager.LoadingScreenHandler"):clearLoaded()
	end
  }
end
