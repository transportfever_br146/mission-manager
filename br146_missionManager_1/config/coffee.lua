local coffee = "https://www.buymeacoff.ee/BR146"

return {
	before = "%abs%",
	de = "Du willst mir 'nen Kaffee ausgeben? Das schätze ich sehr. Hier kannst du dies tun: "..coffee,
	en = "You want to buy me a coffee? I appreciate that. Here you can: "..coffee,
}