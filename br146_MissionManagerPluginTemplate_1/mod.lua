function data()
  return {
  
	info = {
		minorVersion = 0,
		severityAdd = "WARNING",
		severityRemove = "CRITICAL", 
		name = _("MOD-NAME"),
		description = _("MOD-DESC"),

		authors = {
			{
				name = 'BR146',
				role = 'CREATOR',
				text = '',
				steamProfile = '76561198146230194',
				tfnetId = '18313'
			},
		},
		
		tfnetId = 0,
		tags = { "Mission", "Script Mod" },
		
		visible = true,
	},
	runFn = function (settings)
		local MiMaPlugin = require("br146_MissionManagerPlugin_v1_0")
		
		local options = {
			loadingScreenHints = {
				"Loading Mission Manager Template",
				"A foo walks into a bar, takes a look around and says \"Hello World!\".",
				"Real programmers count from 0.",
				"The programmer's wife sent him to the grocery store. Her instructions were: \"Buy butter. See if they have eggs. If they do, buy 10.\" So he bought 10.",
				"I'd like to make the world a better place... but they won't give me the source code.",
				"Ask a programmer to review 10 lines of code, he'll find 10 issues. Ask him to do 500 lines and he'll say it looks good."
			},
			loadingScreenImages = {			
				"template_mission_loadingScreen1.tga",
				"template_mission_loadingScreen2.tga",
			},
			loadingScreenPosters = {
				{
					img = "template_mission_poster.tga",
					txt = "This is a loading screen poster. The text will always appear with the same image. Use it to describe something in the image."
				}
			}
		}
		
		MiMaPlugin.register("br146_MissionManagerPluginTemplate_1", "res/scripts/template_mission.lua", options)
	end,
  }
end
