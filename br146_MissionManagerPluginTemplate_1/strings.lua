function data()
return {
	en = {
		["Idle Clicker"] = "Idle Clicker",
		["Click the correct button and change your score!"] = "Click the correct button and change your score!",
		["There is a reward for doing that :)"] = "There is a reward for doing that :)",
		["i++"] = "i++",
		["i--"] = "i--",
		["Meh"] = "Meh",
		["Only %d times? You can do better!"] = "Only %d times? You can do better!",
		["+1"] = "+1",
		["Gotta pump those numbers up. Those are rookie numbers!"] = "Gotta pump those numbers up. Those are rookie numbers!",
		["MOD-NAME"] = "Mission Manager Plugin Template",
		["MOD-DESC"] = "This is a template for a Mission Manager Plugin. As example, a idle clicker task is implemented.",
	},
	
	de = {
		["Idle Clicker"] = "Idle-Klicker",
		["Click the correct button and change your score!"] = "Klick die Knöpfchen und ändere deine Werte!",
		["There is a reward for doing that :)"] = "Es gibt Belohungen dafür :)",
		["i++"] = "i++",
		["i--"] = "i--",
		["Meh"] = "Meh",
		["Only %d times? You can do better!"] = "Nur %d mal gedrückt? Das kannst du besser!",
		["+1"] = "+1",
		["Gotta pump those numbers up. Those are rookie numbers!"] = "Gotta pump those numbers up. Those are rookie numbers!",
		["MOD-NAME"] = "Mission Manager Plugin Template",
		["MOD-DESC"] = "Das ist ein Plugin Template für den Mission Manager. Als Beispiel wurde ein Idle-Klicker implementiert.",
	}
}
end