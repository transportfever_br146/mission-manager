local missionutil = require("missionutil")

return function(mission, taskManager, medalManager)
	local countForReward	= 10
	local countMedalA		= 5
	local countMedalB		= 20
	local reward			= 1000000
	
	local medalA = medalManager:createMedal( _("Meh"), string.format(_("Only %d times? You can do better!"), countMedalA))
	local medalB = medalManager:createMedal( _("+1"),  _("Gotta pump those numbers up. Those are rookie numbers!"))

	local function makeIdleClickerTask()
		local function inc(taskManager, task)
			taskManager:getUserState().count		= taskManager:getUserState().count			+ 1
			taskManager:getUserState().totalClicks	= taskManager:getUserState().totalClicks	+ 1
			
			return true 
		end
		
		local function dec(taskManager, task)
			taskManager:getUserState().count		= taskManager:getUserState().count			- 1
			taskManager:getUserState().totalClicks	= taskManager:getUserState().totalClicks	+ 1
			
			return true 
		end
		
		local info = {
			name = _("Idle Clicker"),
			paragraphs = {
				{ text = _("Click the correct button and change your score!") }, 
				{ type = "HINT", text = _("There is a reward for doing that :)") },			
			},
			options = {
				{ _("i++"), inc },
				{ _("i--"), dec }
			}
		}

		local task = missionutil.makeTask(info)
		
		task.init = function (taskManager, data)
			taskManager:getUserState().count = 0
			taskManager:getUserState().totalClicks = 0
		end	
		
		task.update = function (taskManager)
			local count			= taskManager:getUserState().count or 0
			local totalClicks	= taskManager:getUserState().totalClicks or 0
		
			if not task.hasMedal(medalA) and totalClicks >= countMedalA then
				task.addMedal(medalA)
			end
			
			if not task.hasMedal(medalB) and totalClicks >= countMedalB then
				task.addMedal(medalB)
			end
		
			if count == countForReward then
				count = 0
				game.interface.book( reward )
			end
			if count == -countForReward then
				count = 0
				game.interface.book( -reward )
			end
			
			task.setProgressText( tostring( count ) .. " / " .. tostring(totalClicks) )
			taskManager:getUserState().count = count
		end

		return task
	end

	taskManager:register("idleClicker", makeIdleClickerTask)
	
	mission.onInit(function()
		taskManager:add("idleClicker")		
	end)
end
