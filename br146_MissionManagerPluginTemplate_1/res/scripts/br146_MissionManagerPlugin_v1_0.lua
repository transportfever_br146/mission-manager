return {
	register = function(modIdent, missionCreationFunction, options)
		-- try loading Mission Manager
		local success, data = pcall(require, "br146_MissionManager.MissionManager")
		
		if success == false then
			-- assume error was caused by missing or disabled Mission Manager
			local msg = string.format("Failed to load BR146's Mission Manager for %q. It must be activated!", modIdent)
			print(msg)
			print("Detailed error:", data)
			return nil
		else
			local API = data.API_1_0
			return API.register(modIdent, missionCreationFunction, options)
		end
	end
}